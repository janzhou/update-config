#!/bin/bash

install=$HOME/Dropbox/projects/update-config/install.sh
tmpdir=/tmp/update-config

update-config(){
config=$1

rm -rf  $tmpdir
mkdir   $tmpdir
cd      $tmpdir

wget https://bitbucket.org/janzhou/$config/raw/master/version
version=$(cat version)
currentversion=$(cat ~/.update-config/$config/version)

[[ $version == $currentversion ]] || $install $config $version
}

case "$1" in
    @world)
        for config in $(ls $HOME/.update-config)
        do
            update-config $config
        done
        ;;
    *)
        update-config $1
        ;;
esac
