#!/bin/bash

urlbase=https://bitbucket.org/janzhou
config=$1
version=$2

tmpdir=/tmp/$config
rm -rf  $tmpdir
mkdir   $tmpdir
cd      $tmpdir

wget $urlbase/$config/downloads/$config-$version.tar.gz
tar -xzf $config-$version.tar.gz
cd $config-$version

~/.update-config/$config/uninstall.sh
./install.sh

[[ -d ~/.update-config ]] || mkdir ~/.update-config

rm -rf  ~/.update-config/$config
mkdir   ~/.update-config/$config
cp uninstall.sh ~/.update-config/$config/uninstall.sh
cp version ~/.update-config/$config/version
